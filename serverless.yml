service: ronnyArvelo-slsApp

provider:
  name: aws
  runtime: nodejs12.x
  region: us-east-1
  stage: dev
  iamRoleStatements:
    - Effect: Allow
      Action:
        - dynamodb:PutItem
        - dynamodb:UpdateItem
      Resource:
        - arn:aws:dynamodb:${self:provider.region}:${aws:accountId}:table/${self:service}-${self:provider.stage}-client_table
    - Effect: Allow
      Action:
        - sns:*
      Resource:
        - arn:aws:sns:${self:provider.region}:${aws:accountId}:${self:service}-${self:provider.stage}-createClientTopic
  environment:
    region: ${self:provider.region}
    clientTable: !Ref clientsDynamoDbTable
    createClientTopic: !Ref createClientTopic


functions:
  createClient:
    handler: create-client/create-client.handler
    description: Receives client information, stores it and notifies of the event
    events:
      - http:
          path: client
          method: post
          cors: true

  createCard:
    handler: create-card/create-card.handler
    description: Creates a card for a client
    events:
      - sqs:
          arn: !GetAtt createCardQueue.Arn

  createGift:
    handler: create-gift/create-gift.handler
    description: Assigns a gift for a client
    events:
      - sqs:
          arn: !GetAtt createGiftQueue.Arn

resources:
  Resources:
    clientsDynamoDbTable:
      Type: AWS::DynamoDB::Table
      Properties:
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        TableName: ${self:service}-${self:provider.stage}-client_table

    createGiftQueue:
      Type: AWS::SQS::Queue
      Properties:
        QueueName: ${self:service}-${self:provider.stage}-createGiftQueue

    createCardQueue:
      Type: AWS::SQS::Queue
      Properties:
        QueueName: ${self:service}-${self:provider.stage}-createCardQueue

    createClientToCreateCardPolicy:
      Type: AWS::SQS::QueuePolicy
      Properties:
        PolicyDocument:
          Statement:
            - Sid: Allow SNS publish to SQS
              Effect: Allow
              Principal:
                Service: sns.amazonaws.com
              Resource:
                - !GetAtt createCardQueue.Arn
              Action: SQS:SendMessage
              Condition:
                ArnEquals:
                  aws:SourceArn: !Ref createClientTopic
        Queues:
          - !Ref createCardQueue

    createClientToCreateGiftPolicy:
      Type: AWS::SQS::QueuePolicy
      Properties:
        PolicyDocument:
          Statement:
            - Sid: Allow SNS publish to SQS
              Effect: Allow
              Principal:
                Service: sns.amazonaws.com
              Resource:
                - !GetAtt createGiftQueue.Arn
              Action: SQS:SendMessage
              Condition:
                ArnEquals:
                  aws:SourceArn: !Ref createClientTopic
        Queues:
          - !Ref createGiftQueue

    createClientTopic:
      Type: AWS::SNS::Topic
      Properties:
        DisplayName: createClientTopic
        TopicName: ${self:service}-${self:provider.stage}-createClientTopic
        Subscription:
          - Protocol: sqs
            Endpoint: !GetAtt createCardQueue.Arn
          - Protocol: sqs
            Endpoint: !GetAtt createGiftQueue.Arn
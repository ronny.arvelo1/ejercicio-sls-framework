const DYNAMODB = require("aws-sdk/clients/dynamodb");
const SNS = require("aws-sdk/clients/sns");
const uuid = require("uuid");

const region = process.env.region

const sns = new SNS({
    region: region,
});

const dynamodb = new DYNAMODB({
    region: region,
});

function calculateAge(birthday) {
    const birthDate = new Date(birthday)// birthday is a string in format YYYYMMDD
    const ageDifMs = Date.now() - birthDate.getTime();
    const ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

exports.handler = async (event) => {
    const body = JSON.parse(event.body);
    console.log('body', body);

    if (!body.dni || !body.name || !body.lastName || !body.birth) {
        return {
            statusCode: 400,
            body: "Must include all attributes",
        };
    }

    if (calculateAge(body.birth) > 65) {
        return {
            statusCode: 400,
            body: "Client must be under 65 years old",
        };
    }

    const id = uuid.v1();

    const dbParams = {
        Item: {
            id: {
                S: id
            },
            dni: {
                S: body.dni,
            },
            name: {
                S: body.name,
            },
            lastName: {
                S: body.lastName,
            },
            birth: {
                S: body.birth,
            },
        },
        ReturnConsumedCapacity: "TOTAL",
        TableName: process.env.clientTable,
    };

    const snsParams = {
        Message: JSON.stringify({id: id,...body}),
        TopicArn: process.env.createClientTopic
    };

    try {
        const dbResult = await dynamodb.putItem(dbParams).promise();
        console.info('dbResult', dbResult);
        const snsResult = await sns.publish(snsParams).promise();
        console.info('snsResult', snsResult);
    } catch (error) {
        console.error(error);
        return {
            statusCode: 500,
            body: error,
        };
    }

    return {
        statusCode: 200,
        body: JSON.stringify({'message': 'Client added successfully'}),
    };
};

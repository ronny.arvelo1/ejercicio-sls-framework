### Quick setup

- install serverless framework
- install npm dependencies
- execute serverless framework deploy

#### Test payload
```JSON
{
"name": "John",
"lastName": "Doe",
"dni": "99554412",
"birth": "1989-10-12"
}
```